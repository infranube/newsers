#!/bin/bash

# set -e

# Evaluar si el comando ping existe, de lo contrario se instala.


apt update && apt install apache2 git -y

deploy_app () {
    cd /newsers && git pull
}

# Si el directorio '/newsers' no existe
# clonarlo

if ! test -d /newsers; then

    git clone https://gitlab.com/infranube/newsers.git /newsers

    deploy_app
else

    # Si el repositorio ya se ha cloando
    # traer posibles cambios de la rama main

    deploy_app
fi

exec "$@"
